SRC = $(wildcard src/*.c)
BIN = $(patsubst src/%.c, bin/%.bin, $(SRC))
DEBUG = $(patsubst src/%.c, debug/%.debug, $(SRC))
FLAGS = -Wall -Wextra -lm

.PHONY:
all: $(BIN)

.PHONY:
debug: $(DEBUG)

zip: MQ3PM4.zip

MQ3PM4.zip: $(SRC) Makefile README.md list.txt
	mkdir -p alkstat_mq3pm4
	cp -rt alkstat_mq3pm4 $^
	zip -r $@ $^
	rm -rf alkstat_mq3pm4

bin/ttest.bin: src/ttest.c src/incbeta/incbeta.c
	mkdir -p bin
	gcc $^ $(FLAGS) -O2 -o $@

bin/%.bin: src/%.c
	mkdir -p bin
	gcc $< $(FLAGS) -O2 -o $@

debug/ttest.debug: src/ttest.c src/incbeta/incbeta.c
	mkdir -p debug
	gcc $^ $(FLAGS) -O2 -o $@

debug/%.debug: src/%.c
	mkdir -p debug
	gcc $< $(FLAGS) -g -o $@

.PHONY:
clean:
	rm -f $(BIN) $(DEBUG)
	rm -rf bin debug
	rm -f MQ3PM4.zip
