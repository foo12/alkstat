#include <stdio.h>
#include <math.h>

double nck(int n, int k) {
  if (k > n) return 1;
  double num = 1;
  double denom = 1;
  for (int i = n; i > n-k; --i) {
    num *= i;
  }
  for (int i = 1; i <= k; ++i) {
    denom *= i;
  }
  return num / denom;
}

int main() {
  int M;
  scanf("%d", &M);

  if (M % 2 == 1) {
    printf("0\n");
    return 0;
  }

  double P = 0;
  for (int i = 0; i <= M/2; ++i) {
    double lefts = nck(M, i);
    double rights = nck(M-i, i);
    double ups = nck(M - (2*i), (M - (2*i))/2);
    P += lefts * rights * ups;
  }

  printf("%lf\n", P/pow(4, M));

  return 0;
}
