#include <stdio.h>

int main() {
  int rows, tmp;
  double N = 1;
  double cols[50];
  double arr[50][2] = {0};
  
  scanf("%d", &rows);
  for (int i = 0; i < rows; ++i) {
    scanf("%lf", &cols[i]);
    N *= cols[i];
  }

  for (int i = 0; i < rows; ++i) {
    arr[i][0] = 0.0f;
    arr[i][1] = 0.0f;
    for (int j = 0; j < cols[i]; ++j) {
      scanf("%d", &tmp);
      if (tmp > 0) {
	++arr[i][0];
      } else if (tmp < 0) {
	++arr[i][1];
      }
    }
  }

  for (long i = 1; i < rows; ++i) {
    double pos = arr[0][0] * arr[i][0] + arr[0][1] * arr[i][1];
    double neg = arr[0][0] * arr[i][1] + arr[0][1] * arr[i][0];
    arr[0][0] = pos;
    arr[0][1] = neg;
  }

  printf("%lf\n", arr[0][0] / N);

  return 0;
}
