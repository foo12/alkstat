#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "incbeta/incbeta.h"

double t_cdf(double t, double v) {
    double x = (t + sqrt(t * t + v)) / (2.0 * sqrt(t * t + v));
    return incbeta(v/2.0, v/2.0, x);
}

int main() {
  double u0, alpha, mode, ux;
  int m = 0;
  double *x = (double*)malloc(sizeof(double) * 10 * 1000);

  if (!x) {
    perror("buy moar memory");
    exit(1);
  }

  scanf("%lf", &u0);
  scanf("%lf", &alpha);
  scanf("%lf", &mode);
  while (scanf("%lf", &x[m++]) != EOF);

  double Sx = 0;
  for (int i = 0; i < m; ++i) Sx += x[i];
  m -= 1;
  ux = Sx / m;

  double s = 0;
  for (int i = 0; i < m; ++i) s += pow((x[i] - ux), 2);
  s = sqrt(s / (m - 1));

  double t = (ux - u0) / (s / sqrt(m));

  int dec;
  if (mode == 0.0) {
    double t_crit = t_cdf((1 - (alpha/2)), m-1);
    dec = (-1 * t_crit) < t && t < t_crit;
  } else {
    double t_crit = mode * t_cdf((1 - alpha), m-1);
    dec = mode * (t_crit - t) > 0;
  }

  printf("%d\n", !dec);

  return 0;
}
