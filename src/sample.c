#include <stdio.h>
#include <stdlib.h>

int cmp(const void *a, const void *b) {
  double xa = *(const double *)a;
  double xb = *(const double *)b;
  return (xa > xb) - (xa < xb);
}

int main() {
  int n, m;
  double x[100];
  double q[100];

  scanf("%d %d", &n, &m);

  for (int i = 0; i < n; ++i) scanf("%lf", &x[i]);
  for (int j = 0; j < m; ++j) scanf("%lf", &q[j]);

  qsort(x, n, sizeof(x[0]), cmp);

  for (int j = 0; j < m; ++j) {
    int k;
    for (k = 0; x[k] < q[j] && k < n; ++k);
    printf("%lf ", (double)k / n);
  }
  printf("\n");
  
  return 0;
}

