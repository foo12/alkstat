#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int cmp(const void *a, const void *b) {
  double da = *(const double *)a;
  double db = *(const double *)b;
  return (da > db) - (da < db);
}

int main() {
  double arr[100];
  int n = 0;
  double u = 0;
  double std = 0;
  double med = 0;

  while (scanf("%lf", &arr[n]) != EOF) ++n;
  qsort(arr, n, sizeof(arr[0]), cmp);

  for (int i = 0; i < n; ++i) {
    u += arr[i];
  }
  u /= (double)n;

  if (n % 2 == 0) {
    med = (arr[(n/2) - 1] + arr[n/2]) / 2.0f;
  } else {
    med = arr[n/2];
  }

  for (int i = 0; i < n; ++i) {
    double tmp = arr[i] - u;
    std += tmp * tmp;
  }

  std *= (double)1 / (n - 1);
  std = sqrt(std);

  printf("%lf %lf %lf\n", u, std, med);

  return 0;
}
