#include <stdio.h>
#include <math.h>

double nck(int n, int k) {
  if (k > n) return 0;
  double num = 1;
  double denom = 1;
  for (int i = n; i > n-k; --i) {
    num *= i;
  }
  for (int i = 1; i <= k; ++i) {
    denom *= i;
  }
  return num / denom;
}

int main() {
  int M;
  scanf("%d", &M);

  if (M % 2 == 1) {
    printf("0\n");
    return 0;
  }

  printf("%lf\n", nck(M, M/2)/pow(2, M));
  
  return 0;
}
