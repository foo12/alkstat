#include <stdio.h>

int main() {
  double N, P, P_curr;
  scanf("%lf %lf", &N, &P);

  int k = 0;
  for (; k <= N; ++k) {
    double P_kk = (k * (k-1)) / (N * (N-1));
    double P_kb = (2 * k * (N-k)) / (N * (N-1));
    P_curr = P_kk + P_kb;
    if (P_curr >= P) break;
  }

  printf("%d\n", k);
  
  return 0;
}
