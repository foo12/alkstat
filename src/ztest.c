#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// https://stackoverflow.com/a/18786808
double std_normal_cdf(double z) {
  return 0.5f * erfc(-z * M_SQRT1_2);
}

int main() {
  double u0, sigma, alpha, mode, ux;
  int m = 0;
  double *x = (double*)malloc(sizeof(double) * 10 * 1000);

  if (!x) {
    perror("buy moar memory");
    exit(1);
  }

  scanf("%lf", &u0);
  scanf("%lf", &sigma);
  scanf("%lf", &alpha);
  scanf("%lf", &mode);
  while (scanf("%lf", &x[m++]) != EOF);
  m -= 1;

  double Sx = 0;
  for (int i = 0; i < m; ++i) Sx += x[i];
  ux = Sx / m;

  double Z = (ux - u0) / (sigma / sqrt(m));

  int dec;
  if (mode == 0.0) {
    double Zcrit = std_normal_cdf(1 - (alpha / 2));
    dec = (-1 * Zcrit) < Z && Z < Zcrit;
  } else {
    double Zcrit = mode * std_normal_cdf(1 - alpha);
    dec = mode * (Zcrit - Z) > 0;
  }

  printf("%d\n", !dec);
  return 0;
}
