#include <stdio.h>

#define ARR_LEN (55 * 6)

int main() {
  int n;
  scanf("%d", &n);
  double buf0[ARR_LEN] = {1, 1, 1, 1, 1, 1, 0};
  double buf1[ARR_LEN] = { 0 };

  double *bbuf;
  double *fbuf = buf0;

  for (int cast_n = 1; cast_n < n; ++cast_n) {
    if (cast_n % 2 == 0) {
      bbuf = buf1;
      fbuf = buf0;
    } else {
      bbuf = buf0;
      fbuf = buf1;
    }

    for (int i = 0; i < ARR_LEN; ++i) {
      double ways = 0;
      for (int cast_val = 1; cast_val <= 6; ++cast_val) {
	if ((i - cast_val) >= 0) ways += bbuf[i - cast_val];
      }
      fbuf[i] = ways;
    }
  }

  double sum = 0;
  for (int i = n; i <= (n * 6); ++i) {
    sum += fbuf[i-1];
  }

  for (int i = n; i <= n * 6; ++i) {
    printf("%e\n", (double)fbuf[i-1] / (double)sum);
  }
  
  return 0;
}
