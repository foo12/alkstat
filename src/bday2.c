#include <stdio.h>

int main() {
  double p = 0, curr = 1;
  int m = 0;
  scanf("%lf", &p);

  for (; (1-curr) < p; ++m) {
    curr *= (double)(365-m)/365;
  }

  printf("%d\n", m);

  return 0;
}
