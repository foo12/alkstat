#include <stdio.h>
#include <stdlib.h>

int main() {
  int A_sz, B_sz;
  int tmp;
  int A_pos = 0;
  int A_neg = 0;
  int B_pos = 0;
  int B_neg = 0;

  scanf("%d %d", &A_sz, &B_sz);

  for (int i = 0; i < A_sz; ++i) {
    scanf("%d", &tmp);
    if (tmp > 0) {
      ++A_pos;
    } else if (tmp < 0) {
      ++A_neg;
    }
  }

  for (int i = 0; i < B_sz; ++i) {
    scanf("%d", &tmp);
    if (tmp > 0) {
      ++B_pos;
    } else if (tmp < 0) {
      ++B_neg;
    }
  }

  double k = A_pos * B_pos + A_neg * B_neg;
  double n = A_sz * B_sz;

  printf("%lf\n", k / n);

  return 0;
}
