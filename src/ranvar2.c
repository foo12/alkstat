#include <stdio.h>
#include <math.h>

int main() {
  int m, n;
  double arr[50*50];
  scanf("%d %d", &m, &n);
  
  for (int i = 0; i < m; ++i) { 
    for (int j = 0; j < n; ++j) {
      scanf("%lf", &arr[i*n+j]);
    }
  }

  // -- CALC --
  double ex = 0;
  double ex2 = 0;
  double exey = 0;

  for (int i = 0; i < m; ++i) {
    double px = 0;
    for (int j = 0; j < n; ++j) {
      px += arr[(i*n) + j];
      exey += i * j * arr[(i*n) + j];
    }
    ex += i * px;
    ex2 += i * i * px;
  }
  double dx = sqrt(ex2 - (ex * ex));

  double ey = 0;
  double ey2 = 0;
  for (int j = 0; j < n; ++j) {
    double py = 0;
    for (int i = 0; i < m; ++i) {
      py += arr[(i*n) + j];
    }
    ey += j * py;
    ey2 += j * j * py;
  }
  double dy = sqrt(ey2 - (ey * ey));

  double cov = exey - (ex * ey);
  double corr = cov / (dx * dy);

  printf("%lf\n", corr);

  return 0;
}
