#include <stdio.h>

int main() {
  double N, K;
  scanf("%lf %lf", &N, &K);

  double P_bb = ((N-K) * (N-K-1)) / (N * (N - 1));
  double P_kk = (K * (K-1)) / (N * (N - 1));
  double P_kb = 1 - P_kk - P_bb;

  double J = P_kk * 3.50 + P_kb * 3.50 + P_bb * 1.0;

  printf("%lf\n", J);

  return 0;
}
