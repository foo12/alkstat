#include <stdio.h>
#include <stdlib.h>

typedef struct {
  double x;
  double p;
} Element;

int cmp(const void *a, const void *b) {
  double xa = (*(const Element *)a).x;
  double xb = (*(const Element *)b).x;
  return (xa > xb) - (xa < xb);
}

int main() {
  int n, m;
  Element arr[100];
  double q[100];

  scanf("%d %d", &n, &m);

  for (int i = 0; i < n; ++i) scanf("%lf", &arr[i].x);
  for (int i = 0; i < n; ++i) scanf("%lf", &arr[i].p);
  for (int j = 0; j < m; ++j) scanf("%lf", &q[j]);

  qsort(arr, n, sizeof(arr[0]), cmp);

  for (int j = 0; j < m; ++j) {
    double curr = 0;
    for (int i = 0; arr[i].x < q[j] && i < n; ++i) curr += arr[i].p;
    printf("%lf ", curr);
  }
  printf("\n");
  
  return 0;
}
