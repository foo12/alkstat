#include <stdio.h>
#include <math.h>

double nck(int n, int k) {
  if (k > n) return 0;
  double num = 1;
  double denom = 1;
  for (int i = n; i > n-k; --i) {
    num *= i;
  }
  for (int i = 1; i <= k; ++i) {
    denom *= i;
  }
  return num / denom;
}

int main() {
  int t;
  scanf("%d", &t);
  double P = 0;
  for (int n2 = 0; (n2 * 2) <= t; ++n2) {
    int n1 = t - (n2 * 2);
    P += pow(0.5f, (n1 + n2)) * nck((n1 + n2), n2);
  }
  printf("%lf\n", P);
  return 0;
}
