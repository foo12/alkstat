#include <stdio.h>
#include <math.h>

int main() {
  int n, M;
  scanf("%d %d", &n, &M);
  if (M > 6 || M == 0) {
    printf("0\n");
    return 0;
  }

  double P = pow((double)M/6, n) - pow(((double)M-1)/6, n);
  
  printf("%lf\n", P);
  return 0;
}
