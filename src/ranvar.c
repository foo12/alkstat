#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct {
  char *buf;
  int sz;
  int i;
} Line;

Line make_line() {
  Line l = {
    .sz = 128,
    .i = 0,
  };
  l.buf = (char*)malloc(sizeof(char) * l.sz);
  if (!l.buf) {
    fprintf(stderr, "Allocation failed!\n");
    exit(1);
  }
  return l;
}

void append(Line *l, char c) {
  if ((l->i + 2) >= l->sz) {
    l->sz *= 2;
    l->buf = (char*)realloc(l->buf, l->sz);
    if (!l->buf) {
      fprintf(stderr, "Allocation failed!\n");
      exit(1);
    }
  }
  l->buf[l->i++] = c;
  l->buf[l->i] = '\0';
}

int main() {
  int sz = 0;
  int v[100] = {0};
  double p[100] = {0};

  // -- READ --
  Line l = make_line();
  char c;
  while ((c = getchar()) != '\n') {
    append(&l, c);
  }

  char *token = strtok(l.buf, " ");
  do {
    v[sz++] = atoi(token);
  } while ((token = strtok(NULL, " ")) != NULL);
    
  l.i = 0;
  while ((c = getchar()) != '\n') {
    append(&l, c);
  }

  sz = 0;
  token = strtok(l.buf, " ");
  do {
    p[sz++] = atof(token);
  } while ((token = strtok(NULL, " ")) != NULL);

  free(l.buf);

  // -- CALC --

  double E = 0;
  for (int i = 0; i < sz; ++i) {
    E += v[i] * p[i];
  }
  double V = 0;
  for (int i = 0; i < sz; ++i) {
    V += pow((v[i] - E), 2) * p[i];
  }

  printf("%lf %lf\n", E, pow(V, 0.5));

  return 0;
}
