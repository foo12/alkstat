#include <stdio.h>
#include <math.h>

// number of subsets 2 ^ n
// number of disjoint subsets 3 ^ n - each element is either in A or B or neither
// 3 ^ n / (2 * 2 ^ n) = 3/4 ^ n

int main() {
  int N;
  scanf("%d\n", &N);
  printf("%lf\n", pow(0.75, N));
  return 0;
}
